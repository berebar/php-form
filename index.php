<?php

//this will only be filled if the user has made a mistake
$errors = array();

//If form has been submitted.
if (isset($_POST['submit'])) {
  //Get the values of our form fields. Using ternary to ensure that the variables exist even if the field was not submitted.
  $firstName = isset($_POST['firstName']) ? $_POST['firstName'] : null;
  $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : null;
  $email = isset($_POST['email']) ? $_POST['email'] : null;
  $phone = isset($_POST['phone']) ? $_POST['phone'] : null;
  $message = isset($_POST['message']) ? $_POST['message'] : null;
  
  //Check the first name and make sure that it isn't a blank/empty string.
  if (strlen(trim($firstName)) === 0) {
    //Blank string, add error to $errors array.
    $errors[] = "You must enter your first name!";
  }
  //Check the last name and make sure that it isn't a blank/empty string.
  if (strlen(trim($lastName)) === 0) {
    //Blank string, add error to $errors array.
    $errors[] = "You must enter your last name!";
  }
  //Make sure that the email address is valid. Leverage PHP's default filter function and checks as described in https://secure.php.net/manual/en/function.filter-var.php and  https://secure.php.net/manual/en/filter.filters.validate.php 
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    //$email is not a valid email. Add error to $errors array.
    $errors[] = "That is not a valid email address!";
  }
  //Make sure comment is not empty
  if (strlen(trim($message)) === 0) {
    //Blank string, add error to $errors array.
    $errors[] = "You must please leave us a comment!";
  }
  //If our $errors array is empty, we can assume that everything went fine.
  if (empty($errors)) {
    // Data formatting
    $fullName    = "{$firstName} {$lastName}";
    $formcontent = " From: $fullName \n Phone: $phone \n Message: $message";
    $recipient   = "mail@mail.com";
    $subject     = "Contact Form";
    $mailheader  = "From: $email \r\n";
    
    // Send email with the data that was submitted and formatted
    mail($recipient, $subject, $formcontent, $mailheader) or die("Uh oh, there's been an error!! Please try again");
    
    // Print a response to the user
    echo "Thank You!";
    
  } else {
    echo '<h1>Error(s)!</h1>';
    //get each of the array's errors
    foreach ($errors as $errorMessage) {
      echo $errorMessage . '<br>';
    }
  }
}
?>

